package ru.t1.panasyuk.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.panasyuk.tm.api.service.model.IUserOwnedService;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        implements IUserOwnedService<M> {

    @Getter
    @NotNull
    @Autowired
    private R repository;

    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final R repository = getRepository();
        repository.add(model);
        return model;
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        @NotNull final R repository = getRepository();
        repository.clear(userId);
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final R repository = getRepository();
        repository.clear();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        @NotNull final R repository = getRepository();
        result = repository.findOneById(userId, id) != null;
        return result;
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @Nullable final List<M> models;
        @NotNull final R repository = getRepository();
        models = repository.findAll();
        return models;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) {
        @Nullable final List<M> models;
        @NotNull final R repository = getRepository();
        models = repository.findAll(userId);
        return models;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final M model;
        @NotNull final R repository = getRepository();
        model = repository.findOneById(userId, id);
        return model;
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M model;
        @NotNull final R repository = getRepository();
        model = repository.findOneByIndex(userId, index);
        return model;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        int result;
        @NotNull final R repository = getRepository();
        result = repository.getSize(userId);
        return result;
    }

    @Nullable
    @Override
    @Transactional
    public M removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M removedModel;
        @NotNull final R repository = getRepository();
        removedModel = repository.findOneById(userId, id);
        if (removedModel == null) throw new EntityNotFoundException();
        repository.remove(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    @Transactional
    public M removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M removedModel;
        @NotNull final R repository = getRepository();
        removedModel = repository.findOneByIndex(userId, index);
        if (removedModel == null) throw new EntityNotFoundException();
        repository.remove(removedModel);
        return removedModel;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) {
        @NotNull final R repository = getRepository();
        repository.clear();
        for (@NotNull final M model : models)
            repository.add(model);
        return models;
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final R repository = getRepository();
        repository.update(model);
    }

}