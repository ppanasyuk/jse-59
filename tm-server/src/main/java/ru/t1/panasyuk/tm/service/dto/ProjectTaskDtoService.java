package ru.t1.panasyuk.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.panasyuk.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.panasyuk.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.exception.field.ProjectIdEmptyException;
import ru.t1.panasyuk.tm.exception.field.TaskIdEmptyException;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @Getter
    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    protected ITaskDtoRepository taskRepository;

    @NotNull
    @Override
    @Transactional
    public TaskDTO bindTaskToProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        boolean isExist = projectRepository.findOneById(userId, projectId) != null;
        if (!isExist) throw new ProjectNotFoundException();
        task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskRepository.update(task);
        return task;
    }

    @Override
    @Transactional
    public ProjectDTO removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable ProjectDTO project;
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.remove(project);
        return project;
    }

    @Override
    @Transactional
    public ProjectDTO removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final ProjectDTO project;
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        if (index > projectRepository.getSize(userId)) throw new IndexIncorrectException();
        project = projectRepository.findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        removeProjectById(userId, project.getId());
        return project;
    }

    @Override
    @Transactional
    public void clearProjects(@NotNull final String userId) {
        @Nullable final List<ProjectDTO> projects;
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        projects = projectRepository.findAll(userId);
        if (projects == null) return;
        for (@NotNull final ProjectDTO project : projects) removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO unbindTaskFromProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        boolean isExist = projectRepository.findOneById(userId, projectId) != null;
        if (!isExist) throw new ProjectNotFoundException();
        task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskRepository.update(task);
        return task;
    }

}