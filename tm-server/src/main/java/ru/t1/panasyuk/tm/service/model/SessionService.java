package ru.t1.panasyuk.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.repository.model.ISessionRepository;
import ru.t1.panasyuk.tm.api.service.model.ISessionService;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {

    @Nullable
    @Override
    public List<Session> findAll() {
        @Nullable final List<Session> sessions;
        @NotNull final ISessionRepository repository = getRepository();
        sessions = repository.findAll();
        return sessions;
    }

    @NotNull
    @Override
    @Transactional
    public Session remove(@Nullable final Session session) {
        if (session == null) throw new EntityNotFoundException();
        @NotNull final ISessionRepository repository = getRepository();
        repository.remove(session);
        return session;
    }

}