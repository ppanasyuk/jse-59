package ru.t1.panasyuk.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResultResponse {

    public AbstractUserResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}