package ru.t1.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.system.ServerAboutRequest;
import ru.t1.panasyuk.tm.dto.request.system.ServerVersionRequest;
import ru.t1.panasyuk.tm.dto.response.system.ServerAboutResponse;
import ru.t1.panasyuk.tm.dto.response.system.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerAboutRequest request
    );

    @NotNull
    @WebMethod
    ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerVersionRequest request
    );

    @WebMethod(exclude = true)
    public static void main(String[] args) {
        ServerAboutResponse response = ISystemEndpoint.newInstance().getAbout(new ServerAboutRequest());
        System.out.println(response.getApplicationName());
        System.out.println(response.getName());
        System.out.println(response.getEmail());
    }

}